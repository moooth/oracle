Create Tablespace first_space
datafile
'/home/oracle/app/oracle/oradata/orcl/first_space_1.dbf'
  SIZE 300M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/first_space2.dbf'
  SIZE 300M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;


Create Tablespace second_space
datafile
'/home/oracle/app/oracle/oradata/orcl/second_space_1.dbf'
  SIZE 300M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/second_space_2.dbf'
  SIZE 300M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

CREATE TABLE lll_products (
  product_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  product_name VARCHAR2(100) NOT NULL,
  description VARCHAR2(500),
  price NUMBER(10, 2) NOT NULL,
  stock NUMBER(10) NOT NULL,
  PRIMARY KEY (product_id)
)
TABLESPACE first_space;

CREATE TABLE lll_customers (
  customer_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  customer_name VARCHAR2(100) NOT NULL,
  email VARCHAR2(100) UNIQUE NOT NULL,
  phone VARCHAR2(20),
  address VARCHAR2(200) NOT NULL,
  PRIMARY KEY (customer_id)
)
TABLESPACE first_space;


CREATE TABLE lll_orders (
  order_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  customer_id NUMBER(10) NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES lll_customers(customer_id),
  order_date DATE NOT NULL,
  total_price NUMBER(10, 2) NOT NULL,
  order_status VARCHAR2(20) NOT NULL,
  PRIMARY KEY (order_id)
)
TABLESPACE first_space
PARTITION BY RANGE (order_date) (
  PARTITION p_order_2020 VALUES LESS THAN (TO_DATE('2021-01-01', 'YYYY-MM-DD')) TABLESPACE first_space,
  PARTITION p_order_2021 VALUES LESS THAN (TO_DATE('2022-01-01', 'YYYY-MM-DD')) TABLESPACE first_space,
  PARTITION p_order_2022 VALUES LESS THAN (TO_DATE('2023-01-01', 'YYYY-MM-DD')) TABLESPACE first_space,
  PARTITION p_order_2023 VALUES LESS THAN (TO_DATE('2024-01-01', 'YYYY-MM-DD')) TABLESPACE first_space,
  PARTITION p_order_future VALUES LESS THAN (MAXVALUE) TABLESPACE first_space
);

CREATE TABLE lll_order_items (
  item_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  order_id NUMBER(10) NOT NULL,
  FOREIGN KEY (order_id) REFERENCES lll_orders(order_id),
  product_id NUMBER(10) NOT NULL,
  FOREIGN KEY (product_id) REFERENCES lll_products(product_id),
  quantity NUMBER(5) NOT NULL,
  price NUMBER(10, 2) NOT NULL,
  PRIMARY KEY (item_id)
)
TABLESPACE first_space;


CREATE TABLE lll_cart (
  cart_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  customer_id NUMBER(10) NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES lll_customers(customer_id),
  product_id NUMBER(10) NOT NULL,
  FOREIGN KEY (product_id) REFERENCES lll_products(product_id),
  quantity NUMBER(5) NOT NULL,
  PRIMARY KEY (cart_id)
)
TABLESPACE first_space;


BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_products (product_name,description, price, stock)
    VALUES ( 'Product ' || i, 'good',ROUND(DBMS_RANDOM.VALUE(1, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_customers (customer_name, email, phone, address)
    VALUES ('Customer ' || i,  LPAD(i, 10, '0') || '@qq.com', '172082' || i, 'address' || i);
  END LOOP;
  COMMIT;
END;


BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_orders (customer_id, order_date, total_price, order_status)
    VALUES (
      ROUND(DBMS_RANDOM.VALUE(1, 50000), 0),
      TRUNC(TO_DATE('2020-01-01', 'YYYY-MM-DD') + DBMS_RANDOM.VALUE(0, 1461)),
      ROUND(DBMS_RANDOM.VALUE(10, 10000), 2),
      'finish'
    );
  END LOOP;
  COMMIT;
END;


BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_order_items (order_id, product_id, quantity, price)
    VALUES (
      i,
      ROUND(DBMS_RANDOM.VALUE(1, 50000), 0),
      ROUND(DBMS_RANDOM.VALUE(1, 100), 0),
      ROUND(DBMS_RANDOM.VALUE(10, 10000), 2)
    );
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_cart (customer_id, product_id, quantity)
    VALUES (
      i,
      TRUNC(DBMS_RANDOM.VALUE(1, 50000)),
      ROUND(DBMS_RANDOM.VALUE(1, 100), 0)
    );
  END LOOP;
  COMMIT;
END;

--创建名为c##admin_user的新用户（您可以根据需要选择其他名称）并设置密码及默认表空间和临时表空间
CREATE USER c##admin IDENTIFIED BY lll123  TEMPORARY TABLESPACE temp;
--为新创建的用户分配表空间限额：
ALTER USER c##admin QUOTA 500M ON users;
--将CONNECT, RESOURCE, 和 DBA角色授予新创建的管理员用户：
GRANT CONNECT, RESOURCE, DBA TO c##admin;


--创建名为c##user_lll的新用户并设置密码及默认表空间和临时表空间：
CREATE USER C##user_lll IDENTIFIED BY lll123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
--为新创建的用户分配表空间限额
ALTER USER C##user_lll QUOTA 200M ON users; 
--将CONNECT角色授予新创建的普通用户
GRANT CONNECT TO C##user_lll;
--为普通用户授予基本的DML（数据操纵语言）操作,如SELECT, INSERT, UPDATE和DELETE：
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_cart  TO C##user_lll;
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_customers  TO C##user_lll;
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_order_items  TO C##user_lll;
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_orders  TO C##user_lll;
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_products  TO C##user_lll;


CREATE OR REPLACE PACKAGE lll_business_logic AS

  -- 添加产品到购物车
  PROCEDURE add_product_to_cart(
    p_customer_id NUMBER,
    p_product_id NUMBER,
    p_quantity NUMBER
  );

  -- 计算购物车总价
  FUNCTION calculate_cart_total(
    p_customer_id NUMBER
  ) RETURN NUMBER;

END lll_business_logic;

#创建包实体
CREATE OR REPLACE PACKAGE BODY lll_business_logic AS

  -- 添加产品到购物车
  PROCEDURE add_product_to_cart(
    p_customer_id NUMBER,
    p_product_id NUMBER,
    p_quantity NUMBER
  )
  IS
  BEGIN
    INSERT INTO lll_cart(customer_id, product_id, quantity)
    VALUES (p_customer_id, p_product_id, p_quantity);
    COMMIT;
  END add_product_to_cart;

  -- 计算购物车总价
  FUNCTION calculate_cart_total(
    p_customer_id NUMBER
  ) RETURN NUMBER
  IS
    v_total NUMBER := 0;
  BEGIN
    SELECT SUM(c.quantity * p.price) INTO v_total
    FROM lll_cart c
    JOIN lll_products p ON c.product_id = p.product_id
    WHERE c.customer_id = p_customer_id;

    RETURN v_total;
  END calculate_cart_total;

END lll_business_logic;


BEGIN
  lll_business_logic.add_product_to_cart(
    p_customer_id => 1,
    p_product_id => 101,
    p_quantity => 2
  );
  COMMIT;
END;
/

DECLARE
  v_total NUMBER;
BEGIN
  v_total := lll_business_logic.calculate_cart_total(p_customer_id => 1);
  DBMS_OUTPUT.PUT_LINE('Total cart value for customer 1: ' || v_total);
END;
/


