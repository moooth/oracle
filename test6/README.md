<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->



学号：202010414307  姓名：李灵丽  班级：3班

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。



### 1.创建表空间
- first_space
```sql
Create Tablespace first_space
datafile
'/home/oracle/app/oracle/oradata/orcl/first_space_1.dbf'
  SIZE 300M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/first_space2.dbf'
  SIZE 300M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```
- second_space

```sql
Create Tablespace second_space
datafile
'/home/oracle/app/oracle/oradata/orcl/second_space_1.dbf'
  SIZE 300M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/second_space_2.dbf'
  SIZE 300M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```
![](../test6/image/pict1.png)

- 创建商品表 (lll_products)：存储商品信息，包括商品ID、名称、描述、价格和库存数量。

product_id（NUMBER(10)）：商品ID（主键）
product_name（VARCHAR2(100)）：商品名称
description（VARCHAR2(500)）：商品描述
price（NUMBER(10, 2)）：商品价格
stock（NUMBER(10)）：库存数量

- 创建顾客表 (lll_customers)：存储顾客信息，包括顾客ID、姓名、邮箱地址、联系电话和送货地址。

customer_id（NUMBER(10)）：顾客ID（主键）
customer_name（VARCHAR2(100)）：顾客姓名
email（VARCHAR2(100)）：邮箱地址（唯一且不能为空）
phone（VARCHAR2(20)）：联系电话
address（VARCHAR2(200)）：送货地址

- 创建订单表 (lll_orders)：存储订单信息，包括订单ID、顾客ID、下单日期、订单总价和订单状态。

order_id（NUMBER(10)）：订单ID（主键）
customer_id（NUMBER(10)）：顾客ID（外键，关联到lll_customers表的customer_id）
order_date（DATE）：下单日期
total_price（NUMBER(10, 2)）：订单总价
order_status（VARCHAR2(20)）：订单状态

- 创建订单明细表 (lll_order_items)：存储订单中每个商品的信息，包括明细ID、订单ID、商品ID、商品数量和商品价格。

item_id（NUMBER(10)）：明细ID（主键）
order_id（NUMBER(10)）：订单ID（外键，关联到lll_orders表的order_id）
product_id（NUMBER(10)）：商品ID（外键，关联到lll_products表的product_id）
quantity（NUMBER(5)）：商品数量
price（NUMBER(10, 2)）：商品价格

- 创建购物车表 (lll_cart)：存储顾客购物车中的商品信息，包括购物车ID、顾客ID、商品ID和商品数量。

cart_id（NUMBER(10)）：购物车ID（主键）
customer_id（NUMBER(10)）：顾客ID（外键，关联到lll_customers表的customer_id）
product_id（NUMBER(10)）：商品ID（外键，关联到lll_products表的product_id）
quantity（NUMBER(5)）：商品数量

### 2.创建表
- product（商品表）
```sql
CREATE TABLE lll_products (
  product_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  product_name VARCHAR2(100) NOT NULL,
  description VARCHAR2(500),
  price NUMBER(10, 2) NOT NULL,
  stock NUMBER(10) NOT NULL,
  PRIMARY KEY (product_id)
)
TABLESPACE first_space;
```
- customer（顾客表）
```sql
CREATE TABLE lll_customers (
  customer_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  customer_name VARCHAR2(100) NOT NULL,
  email VARCHAR2(100) UNIQUE NOT NULL,
  phone VARCHAR2(20),
  address VARCHAR2(200) NOT NULL,
  PRIMARY KEY (customer_id)
)
TABLESPACE first_space;
```

- order（订单表）分区存储
```sql
CREATE TABLE lll_orders (
  order_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  customer_id NUMBER(10) NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES lll_customers(customer_id),
  order_date DATE NOT NULL,
  total_price NUMBER(10, 2) NOT NULL,
  order_status VARCHAR2(20) NOT NULL,
  PRIMARY KEY (order_id)
)
TABLESPACE first_space
PARTITION BY RANGE (order_date) (
  PARTITION p_order_2020 VALUES LESS THAN (TO_DATE('2021-01-01', 'YYYY-MM-DD')) TABLESPACE first_space,
  PARTITION p_order_2021 VALUES LESS THAN (TO_DATE('2022-01-01', 'YYYY-MM-DD')) TABLESPACE first_space,
  PARTITION p_order_2022 VALUES LESS THAN (TO_DATE('2023-01-01', 'YYYY-MM-DD')) TABLESPACE first_space,
  PARTITION p_order_2023 VALUES LESS THAN (TO_DATE('2024-01-01', 'YYYY-MM-DD')) TABLESPACE first_space,
  PARTITION p_order_future VALUES LESS THAN (MAXVALUE) TABLESPACE first_space
);
```

- order_items（订单详情表）
```sql
CREATE TABLE lll_order_items (
  item_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  order_id NUMBER(10) NOT NULL,
  FOREIGN KEY (order_id) REFERENCES lll_orders(order_id),
  product_id NUMBER(10) NOT NULL,
  FOREIGN KEY (product_id) REFERENCES lll_products(product_id),
  quantity NUMBER(5) NOT NULL,
  price NUMBER(10, 2) NOT NULL,
  PRIMARY KEY (item_id)
)
TABLESPACE first_space;
```

- cart（购物车表）
```sql
CREATE TABLE lll_cart (
  cart_id NUMBER(10) GENERATED ALWAYS AS IDENTITY,
  customer_id NUMBER(10) NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES lll_customers(customer_id),
  product_id NUMBER(10) NOT NULL,
  FOREIGN KEY (product_id) REFERENCES lll_products(product_id),
  quantity NUMBER(5) NOT NULL,
  PRIMARY KEY (cart_id)
)
TABLESPACE first_space;
```
![](../test6/image/pict2.png)


### 插入数据
- 商品表
```sql
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_products (product_name,description, price, stock)
    VALUES ( 'Product ' || i, 'good',ROUND(DBMS_RANDOM.VALUE(1, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;
```
![](../test6/image/pict3.png)

- 顾客表
```sql
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_customers (customer_name, email, phone, address)
    VALUES ('Customer ' || i,  LPAD(i, 10, '0') || '@qq.com', '172082' || i, 'address' || i);
  END LOOP;
  COMMIT;
END;
```
![](../test6/image/pict4.png)

- 订单表
```sql
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_orders (customer_id, order_date, total_price, order_status)
    VALUES (
      ROUND(DBMS_RANDOM.VALUE(1, 50000), 0),
      TRUNC(TO_DATE('2020-01-01', 'YYYY-MM-DD') + DBMS_RANDOM.VALUE(0, 1461)),
      ROUND(DBMS_RANDOM.VALUE(10, 10000), 2),
      'finish'
    );
  END LOOP;
  COMMIT;
END;

```
![](../test6/image/pict5.png)

- 订单详情表
```sql
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_order_items (order_id, product_id, quantity, price)
    VALUES (
      i,
      ROUND(DBMS_RANDOM.VALUE(1, 50000), 0),
      ROUND(DBMS_RANDOM.VALUE(1, 100), 0),
      ROUND(DBMS_RANDOM.VALUE(10, 10000), 2)
    );
  END LOOP;
  COMMIT;
END;

```
![](../test6/image/pict6.png)

- 购物车表
```sql
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO lll_cart (customer_id, product_id, quantity)
    VALUES (
      i,
      TRUNC(DBMS_RANDOM.VALUE(1, 50000)),
      ROUND(DBMS_RANDOM.VALUE(1, 100), 0)
    );
  END LOOP;
  COMMIT;
END;
```
![](../test6/image/pict7.png)


### 设计权限及用户分配方案
- 管理员
```sql
--创建名为c##admin_user的新用户（您可以根据需要选择其他名称）并设置密码及默认表空间和临时表空间
CREATE USER c##admin IDENTIFIED BY lll123  TEMPORARY TABLESPACE temp;
--为新创建的用户分配表空间限额：
ALTER USER c##admin QUOTA 500M ON users;
--将CONNECT, RESOURCE, 和 DBA角色授予新创建的管理员用户：
GRANT CONNECT, RESOURCE, DBA TO c##admin;
```
- 普通用户
```sql
--创建名为c##user_lll的新用户并设置密码及默认表空间和临时表空间：
CREATE USER C##user_lll IDENTIFIED BY lll123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
--为新创建的用户分配表空间限额
ALTER USER C##user_lll QUOTA 200M ON users; 
--将CONNECT角色授予新创建的普通用户
GRANT CONNECT TO C##user_lll;
--为普通用户授予基本的DML（数据操纵语言）操作,如SELECT, INSERT, UPDATE和DELETE：
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_cart  TO C##user_lll;
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_customers  TO C##user_lll;
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_order_items  TO C##user_lll;
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_orders  TO C##user_lll;
GRANT SELECT, INSERT, UPDATE, DELETE ON lll_products  TO C##user_lll;
```
![](../test6/image/pict8.png)
![](../test6/image/pict9.png)

### 程序包
```sql
CREATE OR REPLACE PACKAGE lll_business_logic AS

  -- 添加产品到购物车
  PROCEDURE add_product_to_cart(
    p_customer_id NUMBER,
    p_product_id NUMBER,
    p_quantity NUMBER
  );

  -- 计算购物车总价
  FUNCTION calculate_cart_total(
    p_customer_id NUMBER
  ) RETURN NUMBER;

END lll_business_logic;

#创建包实体
CREATE OR REPLACE PACKAGE BODY lll_business_logic AS

  -- 添加产品到购物车
  PROCEDURE add_product_to_cart(
    p_customer_id NUMBER,
    p_product_id NUMBER,
    p_quantity NUMBER
  )
  IS
  BEGIN
    INSERT INTO lll_cart(customer_id, product_id, quantity)
    VALUES (p_customer_id, p_product_id, p_quantity);
    COMMIT;
  END add_product_to_cart;

  -- 计算购物车总价
  FUNCTION calculate_cart_total(
    p_customer_id NUMBER
  ) RETURN NUMBER
  IS
    v_total NUMBER := 0;
  BEGIN
    SELECT SUM(c.quantity * p.price) INTO v_total
    FROM lll_cart c
    JOIN lll_products p ON c.product_id = p.product_id
    WHERE c.customer_id = p_customer_id;

    RETURN v_total;
  END calculate_cart_total;

END lll_business_logic;
```
![](../test6/image/pict10.png)


###  测试
```sql
BEGIN
  lll_business_logic.add_product_to_cart(
    p_customer_id => 1,
    p_product_id => 101,
    p_quantity => 2
  );
  COMMIT;
END;
/

DECLARE
  v_total NUMBER;
BEGIN
  v_total := lll_business_logic.calculate_cart_total(p_customer_id => 1);
  DBMS_OUTPUT.PUT_LINE('Total cart value for customer 1: ' || v_total);
END;
/

```

![](../test6/image/pict11.png)

## 备份方案

1. 备份类型  

完全备份：定期执行完全备份（每周日）。这将备份整个数据库，包括数据文件、控制文件和在线重做日志。
增量备份：在完全备份之间执行增量备份（每天）。这将仅备份自上次完全或增量备份以来更改的数据块。

2. 数据恢复需求  

选择ARCHIVELOG模式：确保数据库运行在归档日志模式下，以便能够捕获所有事务并允许在发生故障时执行点恢复。
3. 备份频率  

每周一次完全备份：每周日晚上在非工作时间执行。
每天一次增量备份：在每个工作日晚上的非工作时间执行。

4. 存储管理  

将备份文件存储在与数据库不同的物理位置以避免单点故障（网络文件服务器或云存储）。
使用Oracle RMAN（恢复管理器）来压缩和加密备份，以节省存储空间并提高安全性。
以下是一个使用Oracle RMAN的备份方案：

配置RMAN：

```sql
CONFIGURE DEFAULT DEVICE TYPE TO DISK;
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '/backup_location/%F';
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 7 DAYS;
# 完全备份脚本（每周日运行）：

RUN {
  BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
}
# 增量备份脚本（每天运行）：

RUN {
  BACKUP INCREMENTAL LEVEL 1 AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
}
```
