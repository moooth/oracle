学号：202010414307   姓名：李灵丽     班级：3班

# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```
## 运行结果

![](../test4/pict1.png)


## 创建YHTriangle的SQL语句
```sql
CREATE OR REPLACE PROCEDURE YHTriangle (N IN INTEGER) IS
    TYPE T_Number IS VARRAY(100) OF INTEGER NOT NULL;
    
    I         INTEGER;
    J         INTEGER;
    Spaces    VARCHAR2(30) := '   ';
    RowArray  T_Number;
BEGIN
    DBMS_OUTPUT.PUT_LINE('1');
    DBMS_OUTPUT.PUT(RPAD(1, N*9, ' '));
    DBMS_OUTPUT.PUT(RPAD(1, N*9, ' '));
    DBMS_OUTPUT.PUT_LINE('');
    
    RowArray := T_Number();
    RowArray.EXTEND(N);
    
    -- 初始化第1和第2行数据
    RowArray(1) := 1;
    RowArray(2) := 1;
    
    FOR I IN 3..N LOOP
        -- 计算当前行元素值并存入数组
        RowArray(I) := 1;
        J := I-1;
        WHILE J > 1 LOOP
            RowArray(J) := RowArray(J) + RowArray(J-1);
            J := J-1;
        END LOOP;
        
        -- 输出当前行
        FOR J IN 1..I LOOP
            DBMS_OUTPUT.PUT(RPAD(RowArray(J), 9, ' '));
        END LOOP;
        DBMS_OUTPUT.PUT_LINE('');
    END LOOP;
END YHTriangle;
/

EXEC YHTriangle(10);  #用此存储过程以生成前10行杨辉三角
```
## 运行图

![](../test4/pict2.png)

## 实验总结

本次实验主要是通过将杨辉三角的代码转化为一个存储过程，并在Oracle数据库中创建、调用该存储过程，从而练习了存储过程在实际工作中的应用。实验总结如下：

- 在存储过程编写前先进行分析,在编写存储过程之前，先仔细阅读了杨辉三角的代码并进行了分析，确认需要使用到的变量和控制流等语句。

- 创建存储过程,根据上述分析，运用了IF...ELSE和FOR循环等语句来实现程序逻辑。在创建存储过程的过程中，需要注意格式和语法的正确性。

- 调用并测试存储过程,为了测试存储过程，我首先需要开启SQL Server端的输出设置并使用SET SERVEROUTPUT ON命令开启客户端的输出，从而将存储过程的输出显示出来。接着，我就可以输入参数并执行存储过程，得到正确的输出结果。

- 通过本次实验，熟悉了创建、编写和使用一个Oracle数据库存储过程的方法，并对SQL语法有了更深入的理解。同时，通过将杨辉三角代码转化为存储过程形式，达到重现利用的效果，提升了数据处理效率和重复性工作量的减少。
- 存储过程是Oracle数据库中重要的编程资源，能够简化操作、增强可靠性并且提高性能。通过学习本次实验所涉及的技术内容，有助于我们更好地应用相关技术，提高我们的工作效率。

